﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverInput : MonoBehaviour
{
    [SerializeField] private Transform m_steerWheelTransform;
    [SerializeField] [Range(0f, 180f)] private float m_maxSteerAngle = 90f;
    [SerializeField] [Range(0f, 1f)] private float m_steerAcceleration = 0.25f;

    private float steerAxis;

    public float SteerAxis
    {
        get { return steerAxis; }
        set { steerAxis = Mathf.Lerp(steerAxis, value, m_steerAcceleration); } //сглаживание движения рулевого колеса
    }

    private Vector2 startSteerWheelPoint;

    private Camera mainCamera;

    // Start is called before the first frame update
    private void Start()
    {
        mainCamera = Camera.main;
        //запоминаем координату рулевого колеса в экранной системе координат
        startSteerWheelPoint = mainCamera.WorldToScreenPoint(m_steerWheelTransform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            //измерение угла между рулем и точкой касания экрана
            float angle = Vector2.Angle(Vector2.up, (Vector2) Input.mousePosition - startSteerWheelPoint);
            angle /= m_maxSteerAngle;
            angle = Mathf.Clamp01(angle);

            if (Input.mousePosition.x > startSteerWheelPoint.x)
                angle *= -1;

            SteerAxis = angle;
        }
        else
        {
            SteerAxis = 0;
        }

        m_steerWheelTransform.localEulerAngles = new Vector3(0f, 0f, SteerAxis * m_maxSteerAngle);
        Vector3 pos = mainCamera.ScreenToWorldPoint(startSteerWheelPoint);
        pos.z = -3;

        m_steerWheelTransform.position = pos;
    }
}
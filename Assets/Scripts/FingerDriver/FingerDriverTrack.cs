﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FingerDriverTrack : MonoBehaviour {
    
    public struct TrackSegment
    {
        public Vector3[] Points;
        public bool IsPointInSegment(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(point, Points[0], Points[1], Points[2]);
        }
    }

    [SerializeField] private LineRenderer m_lineRenderer;
    [SerializeField] private bool m_viewDebug;

    private Vector3[] corners;
    public TrackSegment[] segments;

    private void Start()
    {
        //Заполняем масив опорных точек трассы
        corners = new Vector3[transform.childCount];
        for (int i = 0; i < corners.Length; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            corners[i] = obj.transform.position;
            obj.GetComponent<MeshRenderer>().enabled =
                false; //здесь отключаем рендеринг опорных точек, можно и сами объекты спрятать
        }

        //настраиваем LineRenderer
        m_lineRenderer.positionCount = corners.Length;
        m_lineRenderer.SetPositions(corners);

        //из полученого LineRenderer запекаем меш
        Mesh mesh = new Mesh();
        m_lineRenderer.BakeMesh(mesh, true);

        //создаем массив сегментов трассы (каждый треугольник описан 3-мя вершинами из массива вершин)
        segments = new TrackSegment[mesh.triangles.Length / 3];
        int segmentCounter = 0;
        for (int i = 0; i < mesh.triangles.Length; i += 3)
        {
            segments[segmentCounter] = new TrackSegment();
            segments[segmentCounter].Points = new Vector3[3];
            segments[segmentCounter].Points[0] = mesh.vertices[mesh.triangles[i]];
            segments[segmentCounter].Points[1] = mesh.vertices[mesh.triangles[i + 1]];
            segments[segmentCounter].Points[2] = mesh.vertices[mesh.triangles[i + 2]];
            segmentCounter++;
        }
        
        //отдельно можно продебажить что все точки стали ровно там где нужно и увидеть что треугольники генерируются по порядку
        if (!m_viewDebug)
            return;


        foreach (var segment in segments)
        {
            foreach (var pt in segment.Points)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = pt;
                sphere.transform.localScale = Vector3.one * 0.1f;
            }
        }
    }

    /// <summary>
    /// Определяем находится ли точка в переделах трассы
    /// </summary>
    /// <param name="point">точка</param>
    /// <returns></returns>
    public bool IsPointInTrack(Vector3 point)
    {
        foreach (var segment in segments)
        {
            if (segment.IsPointInSegment(point))
            {
                return true;
            }
        }

        return false;
    }
}

